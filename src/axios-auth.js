import axios from 'axios'
import store from './store'

const url = process.env.VUE_APP_BASE_URL_API

const instance = axios.create({
    baseURL : url,
    timeout: 10000
})

instance.interceptors.request.use(config => {

    try {
        
        if (!window.navigator.onLine) {
            // const message = 'Você está sem conexão com a Internet, favor verificar.'
            // store.dispatch('showSnackbarInfo', message)
            return
        }
    
        // const token = store.getters.getToken

        // if (token && (config.url.indexOf('auth/login') === -1)) {
        //     // instance.defaults.headers.common['Authorization'] = 'Bearer ' + token
        //     config.headers.Authorization = 'Bearer ' + token
        // }
    
        return config

    } catch (error) {
        // console.log(error)
        // const message = 'Aconteceu um error inesperado.'
        // store.dispatch('showSnackbarInfo', message)
    }

})
  
instance.interceptors.response.use(
    config => config,
    (error) => {
        // console.log('Interceptor Response...')
      if (error.response.status === 408 || error.code === 'ECONNABORTED') {
        // console.log(`A timeout happend on url ${error.config.url}`)
        // const message = 'Tempo excedido para obter os dados! Tente novamente.'
        // store.dispatch('showSnackbarInfo', message)
      }
      if (error.response.status === 401) { // Usuario nao logado
        store.dispatch('adm/logout')
        window.location = '/'
      }
      return Promise.reject(error);
    },
);

export default instance
