export const mutations = {
    setDisciplinas(state, disciplinas) {
        state.disciplinas = disciplinas
        window.localStorage.setItem('disciplinas',JSON.stringify(disciplinas))
    },
    addDisciplinas(state, disciplina) {
        let disciplinas = state.disciplinas;
        disciplinas.push(disciplina);
        state.disciplinas = disciplinas
    },
    setResposta(state, disciplina, questao, resposta) {
        let disciplinas = state.disciplinas;
        disciplinas.forEach( d => {
            if ( d.id == disciplina) {
                d.questoes.forEach( q => {
                    if (q.id == questao) {
                        q.resposta = resposta
                    }
                })
            }
        });
        state.disciplinas = disciplinas
        window.localStorage.setItem('disciplinas', JSON.stringify(disciplinas));
    }
};