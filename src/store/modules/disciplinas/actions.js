import dbDisciplina from '@/db/database.json'

export const actions = {
    getDisciplinas(context) {

        let disciplinas = JSON.parse(window.localStorage.getItem('disciplinas'))  || dbDisciplina
        
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve(disciplinas);                
            }, 1000);
        })
        .then( () => {
            context.commit('setDisciplinas', disciplinas);
        })
        // this.axios.get('/api/users')
        //     .then((response) => {
        //         let users = response.data.data;

        //         context.commit('setUsers', users);
        //     });
    },

    addDisciplinas(context, disciplina) {
        context.commit('addDisciplinas', disciplina);
    },

    setResposta(context, disciplina, questao, resposta) {
        context.commit('setResposta', disciplina, questao, resposta);
    }
};