import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
    disciplinas: JSON.parse(window.localStorage.getItem('disciplinas')) || []
};

const namespaced = true;

export const disciplinas = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};