export const getters = {
    message: state => {
        return state.message
    },
    resposta: state => {
        return state.resposta
    },
    ativo: state => {
        return state.ativo
    }
};