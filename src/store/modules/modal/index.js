import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
    message: {
        titulo: '',
        corpo: ''
    },
    resposta: 0,
    ativo: false
};

const namespaced = true;

export const modal = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};