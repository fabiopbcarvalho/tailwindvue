export const mutations = {
    setMessage(state, message) {
        state.message = message
    },
    setResposta(state, resposta) {
        state.resposta = resposta
    },
    setAtivo(state, ativo) {
        state.ativo = ativo
    }  
};