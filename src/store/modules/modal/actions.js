export const actions = {
    setMessage(context, message) {
        context.commit('setMessage', message);
    },
    setResposta(context, resposta) {
        context.commit('setResposta', resposta)
    },
    setAtivo(context, ativo) {
        context.commit('setAtivo', ativo)
    }
};