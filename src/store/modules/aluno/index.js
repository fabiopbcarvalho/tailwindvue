import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
    aluno: JSON.parse(window.localStorage.getItem('aluno')) || null
};

const namespaced = true;

export const aluno = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};