export const actions = {
    setEtapa1(context, dados) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve('ok');                
            }, 1000);
        })
        .then( () => {
            context.commit('setEtapa1', dados);
        })
    },
    setEtapa2(context, dados) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve('ok');                
            }, 1000);
        })
        .then( () => {
            context.commit('setEtapa2', dados);
        })
    },
    setEtapa3(context, dados) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve('ok');                
            }, 1000);
        })
        .then( () => {
            context.commit('setEtapa3', dados);
        })
    },
    setEtapa4(context, dados) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve('ok');                
            }, 1000);
        })
        .then( () => {
            context.commit('setEtapa4', dados);
        })
    },
    setEtapa5(context, dados) {
        return new Promise((resolve) => {
            setTimeout(() => {
                resolve('ok');                
            }, 1000);
        })
        .then( () => {
            context.commit('setEtapa5', dados);
        })
    },
};