export const mutations = {
    setEtapa1(state, dados) {
        state.aluno.urlFoto         = dados.urlFoto        
        state.aluno.nomeAluno       = dados.nomeAluno      
        state.aluno.nomeMae         = dados.nomeMae        
        state.aluno.nomePai         = dados.nomePai        
        state.aluno.dataNascto      = dados.dataNascto     
        state.aluno.matriculaSigeam = dados.matriculaSigeam
        state.aluno.sexo            = dados.sexo           
        state.aluno.idCor           = dados.idCor          
        state.aluno.idEstadoCivil   = dados.idEstadoCivil  
        state.aluno.idNacionalidade = dados.idNacionalidade
        state.aluno.idPaisOrigem    = dados.idPaisOrigem   
        state.aluno.dataEntradaPais = dados.dataEntradaPais
        state.aluno.uf              = dados.uf             
        state.aluno.municipio       = dados.municipio      
        window.localStorage.setItem('aluno', JSON.stringify(state.aluno))
    },
    setEtapa2(state, dados) {
        state.aluno.idCertidao          = dados.idCertidao         
        state.aluno.ufCartoriodCertidao = dados.ufCartoriodCertidao
        state.aluno.dataEmissao         = dados.dataEmissao        
        state.aluno.idMunicipioCartorio = dados.idMunicipioCartorio
        state.aluno.cartorio            = dados.cartorio
        state.aluno.termo               = dados.termo   
        state.aluno.folha               = dados.folha   
        state.aluno.livro               = dados.livro   
        window.localStorage.setItem('aluno', JSON.stringify(state.aluno))
    },
    setEtapa3(state, dados) {
        state.aluno.idMunicipioAluno = dados.idMunicipioAluno 
        state.aluno.idBairroAluno    = dados.idBairroAluno    
        state.aluno.logradouro       = dados.logradouro       
        state.aluno.numero           = dados.numero           
        state.aluno.cep              = dados.cep              
        state.aluno.complemento      = dados.complemento      
        state.aluno.ddd              = dados.ddd              
        state.aluno.telefone         = dados.telefone         
        state.aluno.celular          = dados.celular          
        state.aluno.email            = dados.email            
        window.localStorage.setItem('aluno', JSON.stringify(state.aluno))
    },
    setEtapa4(state, dados) {
        state.aluno.cpf          = dados.cpf          
        state.aluno.senha        = dados.senha
        window.localStorage.setItem('aluno', JSON.stringify(state.aluno))
    },
    setEtapa(state, dados) {
        state.aluno.nomeResponsavel = dados.nomeResponsavel
        state.aluno.grauParentesco  = dados.grauParentesco 
        window.localStorage.setItem('aluno', JSON.stringify(state.aluno))
    },
};