export const actions = {
    login(context, login) {
        return new Promise((resolve) => {
            setTimeout(() => {
                context.commit('setLogin', login);
                resolve(true);                
            }, 1000);
        })
    },
    
    logout(context) {
        context.commit('setLogout');
    }
};