export const mutations = {
    setLogin(state, login) {
        state.login = login
        window.localStorage.setItem('login', JSON.stringify(login))
    },
    setLogout(state) {
        state.login = null
        window.localStorage.removeItem('login')
    }
};