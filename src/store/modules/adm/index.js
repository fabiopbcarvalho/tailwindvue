import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
    login: JSON.parse(window.localStorage.getItem('login')) || null
};

const namespaced = true;

export const adm = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};