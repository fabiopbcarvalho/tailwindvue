export const mutations = {
    setUsers(state, users) {
        state.users = users
        window.localStorage.setItem('users', JSON.stringify(users))
    },
    addUser(state, user) {
        let users = state.users;
        users.push(user);
        state.users = users
        window.localStorage.setItem('users', JSON.stringify(users))
    },
};