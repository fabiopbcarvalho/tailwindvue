export const getters = {
    user: state => {
        const users = state.users 
        return (users) ? users[0] : null
    }
};