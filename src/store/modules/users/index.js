import {getters} from './getters'
import {actions} from './actions'
import {mutations} from './mutations'

const state = {
    users: JSON.parse(window.localStorage.getItem('users')) || []
};

const namespaced = true;

export const users = {
    namespaced,
    state,
    getters,
    actions,
    mutations
};