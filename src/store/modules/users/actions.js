import dbUser from '@/db/dbUsers.json'

export const actions = {
    getUsers(context) {
        return new Promise(response,reject)
        .then( (response) => {
            let users = dbUser
            context.commit('setUsers', users);
        })
        // this.axios.get('/api/users')
        //     .then((response) => {
        //         let users = response.data.data;

        //         context.commit('setUsers', users);
        //     });
    },
    addUser(context, user) {
        context.commit('addUser', user);
    },
    setUsers(context, users) {
        context.commit('setUsers', users);
    }
};