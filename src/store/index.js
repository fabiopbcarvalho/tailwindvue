import Vue from 'vue'
import Vuex from 'vuex'
import {adm} from './modules/adm'
// import {users} from './modules/users'
// import {disciplinas} from './modules/disciplinas';
// import {modal} from './modules/modal'

// import ResumoJSON from '@/db/resultado.json'

Vue.use(Vuex);

export default new Vuex.Store({
    
  modules: {
    adm
      // users,
      // disciplinas,
      // modal,
  },

  state: {
    loading: false
  },

  getters: {
    loading: state => state.loading
  },

  mutations: {
    setLoading(state, payload) {
      state.loading = payload
    }
  },
  
  actions: {
    setLoading({commit}, payload) {
      commit('setLoading', payload)
    },
    // temPendencia({}, payload) {
    //     return new Promise((resolve) => {
    //         setTimeout(() => {
    //           resolve(ResumoJSON)
    //         }, 50)
    //       })
    //     //const url = ''
    //     //return axios.post(url, payload)
    // },
    // resultadoProva({}, payload) {
    //     return new Promise((resolve) => {
    //         setTimeout(() => {
    //           resolve(ResumoJSON)
    //         }, 50)
    //       })
    //     //const url = ''
    //     //return axios.post(url, payload)
    // }
  }
});